﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace luokkaharjoitukset
{
    class Time
    {
        int hour, minute, second;

        public Time(int hour, int minute, int second)
        {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        }

        public Time nextSecond()
        {
            this.second++;

            if (this.second > 59)
            {
                this.minute++;
                this.second = 0;
            }

            if (this.minute > 59)
            {
                this.hour++;
                this.minute = 0;
            }

            if (this.hour > 23)
            {
                this.hour = 0;
            }

            return this;
        }

        public Time previousSecond()
        {
            this.second--;

            if (this.second<0)
            {
                this.minute--;
                this.second = 59;
            }

            if (this.minute<0)
            {
                this.hour--;
                this.minute = 59;
            }

            if (this.hour<0)
            {
                this.hour = 23;
            }

            return this;
        } 

        public int getHour()
        {
            return this.hour;
        }

        public int getMinute()
        {
            return this.minute;
        }

        public int getSecond()
        {
            return this.second;
        }

        public void setHour(int hour)
        {
            this.hour = hour;
        }

        public void setMinute(int minute)
        {
            this.minute = minute;
        }

        public void setSecond(int second)
        {
            this.second = second;
        }

        public void setTime(int hour, int minute, int second)
        {
            this.hour = hour;
            this.minute = minute;
            this.second = second;
        }

        public override String ToString()
        {
            return this.hour.ToString("D2") + ":" + this.minute.ToString("D2") + ":" + this.second.ToString("D2");
        }
    }
}
