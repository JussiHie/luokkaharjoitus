﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace luokkaharjoitukset
{
    class InvoiceItem
    {
        string id, desc;
        int qty;
        double unitPrice;

        public InvoiceItem(string id, string desc, int qty, double unitPrice)
        {
            this.id = id;
            this.desc = desc;
            this.qty = qty;
            this.unitPrice = unitPrice;
        }

        public string getID()
        {
            return this.id;
        }

        public string getDesc()
        {
            return this.desc;
        }

        public int getQty()
        {
            return this.qty;
        }

        public double getUnitPrice()
        {
            return this.unitPrice;
        }

        public double getTotal()
        {
            return this.qty * this.unitPrice;
        }

        public void setQty(int qty)
        {
            this.qty = qty;
        }

        public void setUnitPrice (double unitPrice)
        {
            this.unitPrice = unitPrice;
        }

        public override String ToString()
        {
            return "Viivakoodi: " + id + " nimi:" + desc + " määrä: " + qty + " yksikköhinta: " + unitPrice + " ja kokonaissumma: " + getTotal();
        }
    }
}
