﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace luokkaharjoitukset
{
    class Rectangle
    {
        float length, width;

        public Rectangle()
        {
            this.length = 1.0F;
            this.width = 1.0F;
        }

        public Rectangle(float length, float width)
        {
            this.length = length;
            this.width = width;
        }

        public float getLength()
        {
            return length;
        }

        public float getWidth()
        {
            return width;
        }

        public float getArea()
        {
            return width * length;
        }

        public float getPerimeter()
        {
            return width * 2 + length * 2;
        }

        public void setLength(float length)
        {
            this.length = length;
        }

        public void setWidth(float width)
        {
            this.width = width;
        }

        public override String ToString()
        {
            return "Suorakulmion leveys: " + this.width + " ja pituus " + this.length;
        }
    }
}
