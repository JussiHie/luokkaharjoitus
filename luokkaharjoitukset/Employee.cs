﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace luokkaharjoitukset
{
    class Employee
    {
        int id, salary;
        string firstName, lastName;

        public Employee(int id, string firstName, string lastName, int salary)
        {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.salary = salary;
        }

        public void raiseSalary(int percent)
        {
            this.salary = Convert.ToInt32(this.salary + ((double)salary/100.0) * percent);

        }

        public int getId()
        {
            return this.id;
        }

        public string getFirstName()
        {
            return this.firstName;
        }

        public string getLastName()
        {
            return this.lastName;
        }

        public int getSalary()
        {
            return this.salary;
        }

        public int getAnnualSalary()
        {
            return this.salary * 12;
        }

        public void setSalary(int salary)
        {
            this.salary = salary;
        }

        public override String ToString()
        {
            return "Työntekijä " + this.id + ", " + this.firstName + " " + this.lastName + ", " + this.salary;
        }
    }
}
