﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace luokkaharjoitukset
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle c1 = new Circle();

            Console.WriteLine("The circle 1 has radius of " + c1.getRadius() + " and area of " + c1.getArea());

            Circle c2 = new Circle(2.0);

            Console.WriteLine("The circle 2 has radius of " + c2.getRadius() + " and area of " + c2.getArea());

            Circle c3 = new Circle(1.0, "pink");

            Console.WriteLine("Ympyrän 3 säde on " + c3.getRadius() + ", pinta-ala on " + c3.getArea() + " ja väri on " + c3.getColor());

            Circle c4 = new Circle(1.5, "yellow");

            Console.WriteLine("Ympyrän 4 säde on " + c4.getRadius() + ", pinta-ala on " + c4.getArea() + " ja väri on " + c4.getColor());

            c4.setRadius(2);
            c4.setColor("blue");

            Console.WriteLine("Mutta nyt nelosen säde on " + c4.getRadius() + ", pinta-ala on " + c4.getArea() + " ja väri on " + c4.getColor() + ". Hämmästyttävää!");

            Console.WriteLine("Kokeillaas toStringiä! Kutsutaan sitä ensin tarkasti ja sen jälkeen vain olion nimellä");
            Console.WriteLine(c4.ToString());
            Console.WriteLine(c4);
            Console.WriteLine("Ja tulostushan on aivan sama. :O");

            Console.WriteLine("Ympyrän 4 kehän pituus on " + c4.getCircumference());

            newLines(2);

            Rectangle r1 = new Rectangle();

            Console.WriteLine("Suorakulmio 1:n pituus on " + r1.getLength() + " ja leveys on " + r1.getWidth());

            r1.setLength(1.5F);
            r1.setWidth(2F);

            Console.WriteLine("Suorakulmio 1:n pituus onkin nyt " + r1.getLength() + " ja leveys on " + r1.getWidth());

            Rectangle r2 = new Rectangle(3F, 1.75F);
            Console.WriteLine("Suorakulmio 2:n pituus on " + r2.getLength() + " ja leveys on " + r2.getWidth());
            Console.WriteLine("Suorakulmio 2:n pinta-ala on " + r2.getArea() + " ja ympärysmitta on " + r2.getPerimeter());
            Console.WriteLine("Ja testaillaan toStringiä");
            Console.WriteLine(r2);

            newLines(2);

            Employee emp1 = new Employee(1337, "Matti", "Meikäläinen", 2000);

            Console.WriteLine("Työntekijä " + emp1.getFirstName() + " " + emp1.getLastName() + " lisätty. ID on " + emp1.getId() + " ja liksa " + emp1.getSalary());
            Console.WriteLine(emp1.getFirstName() + " on tehnyt hyvää työtä. Nostetaan liksa 3500.");
            emp1.setSalary(3500);
            Console.WriteLine(emp1.getFirstName() + " vetää nyt " + emp1.getSalary() + " liksaa. Täten hän saa vuodessa " + emp1.getAnnualSalary());
            Console.WriteLine(emp1.getFirstName() + " onnistui projektissa erinomaisesti. Annetaan 5%:n palkankorotus");
            emp1.raiseSalary(5);
            Console.WriteLine(emp1.getFirstName() + " on onnellinen kun saa " + emp1.getSalary() + " liksaa");
            Console.WriteLine(emp1.getFirstName() + "! Nyt tuli töpeksittyä kunnolla. Liksasta lähtee heti 50%!");
            emp1.raiseSalary(-50);
            Console.WriteLine(emp1.getFirstName() + " ei ole niin onnellinen kun saa vain " + emp1.getSalary() + " liksaa");
            Console.WriteLine("Aika kokeilla toStringiä");
            Console.WriteLine(emp1);

            newLines(2);

            InvoiceItem item1 = new InvoiceItem("6414890306007", "Herneitä", 1, 1.99);
            Console.WriteLine("Kuittiin lisätty " + item1.getDesc() + ", jonka viivakoodi on " + item1.getID() + ". Niitä on " + item1.getQty() + " kpl ja yksikköhinta on " + item1.getUnitPrice() + ". Yhteensä " + item1.getDesc() + " maksavat " + item1.getTotal());
            Console.WriteLine("Hihnaa myöten tulee vähän lisää " + item1.getDesc() + "...");
            item1.setQty(10);
            Console.WriteLine("Nyt kuitissa on " + item1.getQty() + " kpl " + item1.getDesc() + ". Hintaa niillä yksittäin on " + item1.getUnitPrice() + " ja yhteensä " + item1.getTotal());
            Console.WriteLine("Annetaan vähän paljousalennusta ja tiputetaan hinta 1,49");
            item1.setUnitPrice(1.49);
            Console.WriteLine("Mitä tästä sanoo toString?");
            Console.WriteLine(item1);
            Console.WriteLine("Aivan oikein!");

            newLines(2);

            Console.WriteLine("Luodaan Matille ja Maijalle tilit");
            Account act1 = new Account("1", "Matti Meikäläinen");
            Account act2 = new Account("2", "Maija Meikäläinen", 3000);
            Console.WriteLine("ID-tunnuksen " + act1.getID() + " tilin omistaa " + act1.getName() + " ja tilillä on rahaa " + act1.getBalance());
            Console.WriteLine("Ja toString sanoo Maijan tilistä:");
            Console.WriteLine(act2);
            Console.WriteLine("Lisätääs Maijan tilille 200. Nyt Maijan tilillä on " + act2.credit(200));
            Console.WriteLine("Maija ostaa 700 maksavan polkupyörän. Tilille jää " + act2.debit(700));
            Console.WriteLine("Matti haluaa kanssa ostaa polkupyörän...");
            int temp = act1.debit(700);
            Console.WriteLine("Valitettavasti Matin tilillä on vain " + temp);
            Console.WriteLine("Maija siirtää rahaa Matin tilille, että hän voi ostaa polkupyörän");
            Console.WriteLine("Täten Maijan tilille jää " + act2.transferTo(act1, 710));
            Console.WriteLine(act1);
            Console.WriteLine("Matti ostaa fillarin ja tilille jää " + act1.debit(700));
            Console.WriteLine("Matti siirtää Maijan tilille 10 ja Matin tilille jää " + act1.transferTo(act2, 10));
            Console.WriteLine(act1);
            Console.WriteLine(act2);
            Console.WriteLine("Matti yrittää siirtää lisää rahaa Maijan tilille, mutta...");
            temp = act1.debit(30);
            Console.WriteLine("Ei onnaa, koska Matilla on " + temp + " tilillä.");
            Console.WriteLine(act1);
            Console.WriteLine(act2);

            newLines(2);

            Date d1 = new Date(1, 1, 2018);
            Date d2 = new Date(21, 11, 2017);
            Console.WriteLine("Päivämäärä 1 on " + d1.getDay() + "." + d1.getMonth() + "." + d1.getYear());
            Console.WriteLine("Päivämäärä 2 on " + d2.getDay() + "." + d2.getMonth() + "." + d2.getYear());
            Console.WriteLine("Vaihdetaan päivämäärä 1");
            d1.setDay(4);
            d1.setMonth(3);
            d1.setYear(2017);
            Console.WriteLine("Päivämäärä 1 on " + d1.getDay() + "." + d1.getMonth() + "." + d1.getYear());
            Console.WriteLine("Vaihdetaan päivämäärä 2");
            d2.setDate(20, 10, 2015);
            Console.WriteLine("Päivämäärä 2 on " + d2.getDay() + "." + d2.getMonth() + "." + d2.getYear());
            Console.WriteLine("Ja miltäs nämä näyttävät toStringillä?");
            Console.WriteLine(d1);
            Console.WriteLine(d2);

            newLines(2);

            Time t1 = new Time(16, 46, 33);
            Time t2 = new Time(23, 59, 59);
            Time t3 = new Time(6, 2, 7);
            Console.WriteLine("Aika 1 on " + t1.getHour() + " tuntia, " + t1.getMinute() + " minuuttia ja " + t1.getSecond() + " sekuntia");
            Console.WriteLine("Ja toStringillä ilmaistuna");
            Console.WriteLine("Aika 1: " + t1);
            Console.WriteLine("Aika 2: " + t2);
            Console.WriteLine("Aika 3: " + t3);
            Console.WriteLine("Säädetääs vähän kelloja.");
            t3.setHour(8);
            t3.setMinute(5);
            t3.setSecond(2);
            Console.WriteLine("Aika 3: " + t3);
            t1.setTime(19, 31, 20);
            Console.WriteLine("Aika 1: " + t1);
            Console.WriteLine("Siirretään kelloa 2 eteenpäin");
            Console.WriteLine("Aika 2: " + t2);
            Console.WriteLine("Aika 2: " + t2.nextSecond());
            Console.WriteLine("Aika 2: " + t2.nextSecond());
            Console.WriteLine("Aika 2: " + t2.nextSecond());
            Console.WriteLine("Ja sitten taaksepäin");
            Console.WriteLine("Aika 2: " + t2.previousSecond());
            Console.WriteLine("Aika 2: " + t2.previousSecond());
            Console.WriteLine("Aika 2: " + t2.previousSecond());
            Console.WriteLine("Aika 2: " + t2.previousSecond());
        }

        public static void newLines(int count)
        {
            for (int i=0; i<count; i++)
            {
                Console.WriteLine();
            }
        }
    }
}
