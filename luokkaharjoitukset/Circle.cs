﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace luokkaharjoitukset
{
    class Circle
    {
        double radius;
        string color;

        public Circle()
        {
            this.radius = 1.0;
            this.color = "red";
        }

        public Circle(double radius)
        {
            this.radius = radius;
            this.color = "red";
        }

        public Circle(double radius, string color)
        {
            this.radius = radius;
            this.color = color;
        }

        public double getRadius()
        {
            return radius;
        }

        public double getArea()
        {
            return radius * radius * Math.PI;
        }

        public string getColor()
        {
            return color;
        }

        public double getCircumference()
        {
            return 2 * radius * Math.PI;
        }

        public void setRadius(double newRadius)
        {
            radius = newRadius;
        }

        public void setColor(String newColor)
        {
            color = newColor;
        }

        public override String ToString()
        {
            return "Circle[radius=" + radius + " color=" + color + "]";
        }

    }

}
