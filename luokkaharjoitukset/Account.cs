﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace luokkaharjoitukset
{
    class Account
    {
        string id, name;
        int balance;

        public Account(string id, string name)
        {
            this.id = id;
            this.name = name;
            this.balance = 0;
        }

        public Account(string id, string name, int balance)
        {
            this.id = id;
            this.name = name;
            this.balance = balance;
        }

        public int credit(int amount)
        {
            this.balance += amount;
            return balance;
        }

        public int debit(int amount)
        {
            if (amount <= this.balance)
            {
                this.balance -= amount;
            } else
            {
                Console.WriteLine("Amount exceeded balance.");
            }

            return balance;
        }

        public int transferTo(Account toAccount, int amount)
        {
            if (amount <= this.balance)
            {
                toAccount.credit(amount);
                this.debit(amount);
            }
            else
            {
                Console.WriteLine("Amount exceeded balance.");
            }

            return this.balance;

        }

        public string getID()
        {
            return this.id;
        }

        public string getName()
        {
            return this.name;
        }

        public int getBalance()
        {
            return this.balance;
        }

        public override String ToString()
        {
            return "Account id=" + this.id + ", name=" + this.name + ", balance=" + this.balance;
        }
    }
}
